import gsap from "gsap";

function feebackAni(className) {
  let tl = gsap.timeline({
    // delay: 0.25,    
    // repeat: -1,
    // repeatDelay: 1
    onComplete: function () {
      document.querySelector('.feekback__box').classList.remove(className);
      document.body.classList.add('done');
    }
  })
  tl.addLabel('Start')
    .from('.feekback', {
      transformOrigin: "center",
      duration: 1,
      opacity: 0,
      ease: "elastic.out(1, 0.5)",
      clearProps: 'all' //動畫結束后清空style屬性
    }, 'Start')
    .from('.feekback__box', {
      scale: 0,
      duration: 1,
      opacity: 0,
      ease: "elastic.out(1, 0.3)",
      clearProps: 'all' //動畫結束后清空style屬性
    }, 'Start+=0.25')
}



export {
  feebackAni
};