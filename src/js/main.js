// import $ from 'jquery';
import Splide from '@splidejs/splide';
import Swal from 'sweetalert2';
import gsap from "gsap";
import {
  feebackAni
} from './animations.js';

import VConsole from 'vconsole';
const vConsole = new VConsole({
  maxLogNumber: 1000
});

(function () {
  var Images = [
    './images/background/bg_blue.png',
    './images/background/bg_red.png',
    './images/background/bg_green.png',
    './images/background/bg_classroom2.jpg',
    './images/background/bg_green.png',
    './images/background/bg_rain_school.png',
    './images/background/bg_school.png',
    './images/background/bg_snow_school.png',
    './images/background/bg_yuyake_school.png',
    './images/background/lightbox_bg.png',
    './images/background/record_bg.png',
    './images/components/close_bg.png',
  ];
  $.fn.MainDataInIt = function () {
    $.preload(Images, 1, function (last) {
      if (last) {
        vConsole
        init();
        loadAni();
        InstructionSplide.mount();
      }
    });
  };
  $('.play').innerHeight();
  $('.close_splide').on('click', function (e) {
    e.preventDefault();
    HomeSplide.mount();
    document.querySelector('.instructionsplide').style.display = 'none';
  })

  function isMobile() {
    try {
      document.createEvent("TouchEvent");
      return true;
    } catch (e) {
      return false;
    }
  };

  var init = function () {
    if (isMobile()) {
      var mql = window.matchMedia("(orientation: portrait)");
      if (mql.matches) { // 直立
        $('.tips').show();
      }
    };
    $('.enter').on('click', function () {
      $('.tips').fadeOut();
      location.reload();
    })
  }

  var InstructionSplide = new Splide('.instructionsplide', {
    ttype: "fade",
    pagination: false,
    focus: 'center',
    width: '100%',
    perPage: 1,
  });

  var HomeSplide = new Splide('.splide', {
    type: "fade",
    pagination: false,
    perPage: 1,
    focus: 'center',
  });

  let click_btn = document.querySelector('#click');
  let right_btn = document.querySelector('#answer');
  let fail_btn = document.querySelector('#wrong');
  let _time = 10;
  let interval;
  let score;
  let ShowNum = 0 //學習題數
  var scoreNum = 0;
  var count = 0; //現在題數
  var json = "http://122.116.13.245:41025/en-games/api/stage/stage";
  // var json = "json/index.json";
  var stageWords; //該關卡單字
  let stageData = { //該關卡資料
    userID: '',
    stage: '',
    level: '',
    score: ''
  };
  let userData = { //使用者資料
    userID: '',
    name: '',
    account: '',
    psw: '',
  }
  let questNum = []; //亂數題目順序(normal/hard)
  let userAns = []; //使用者答案列(normal/hard)
  var CloseLightBox = function (lbHref) {
    $(".lightbox").removeClass("is-active");
    $(lbHref).removeClass('is-active');
    $("body").removeClass('noscroll');
    return false;
  };

  function OpenLightBox(lbHref) {
    $(".lightbox__close").click(function () {
      CloseLightBox();
      return false;
    });

    $(".lightbox").addClass("is-active");
    $(lbHref).addClass('is-active');
    $("body").addClass('noscroll');
    return false;
  };

  function ChangePage(page) {
    CloseLightBox('#Stage');
    $('.splide').hide();
    $('.play').hide();
    switch (page) {
      case '#home':
        $('#home').show();
        break;
      case '#easy':
        $('.round_first').show().addClass('play');
        break;
      case '#normal':
        $('.round_second').show().addClass('play');
        break;
      case '#hard':
        $('.round_third').show().addClass('play');
        break;
      case '#learnbox':
        $('#learnbox').show().addClass('play');
        break;
    }
  }

  function ClickEvent(e) {
    $.Body.on(
      "click",
      ".lb-open , .stage-item , .round-back , .RunNext , .learn__replay",
      function (e) {
        var _click = $(this);
        switch (true) {
          case _click.hasClass("lb-open"):
            e.preventDefault();
            var lbHref = $(this).attr('href');
            OpenLightBox(lbHref);

            stageData.stage = $(this).find(".number").text();
            stageData.score = '';
            // 跳窗顯示等級分數&星數
            $.post("http://122.116.13.245:41025/en-games/api/stage/stage", stageData).done(function (data) {
              console.log(data)
              if (data === '[]') {
                data = [{
                  score: 0
                }, {
                  score: 0
                }, {
                  score: 0
                }, ]
              } else {
                if (data.length != 3) {
                  let allLv = ['0', '1', '2'];
                  let lvs = data.map(lv => lv.level);
                  lvs.forEach(lv => {
                    console.log(allLv.includes(lv))
                    if (allLv.includes(lv)) {
                      let idx = allLv.indexOf(lv);
                      allLv.splice(idx, 1);
                    }
                  })
                  allLv.forEach(lv => {
                    data.push({
                      level: lv,
                      score: 0
                    })
                  })
                }
              }
              //分數轉換星數
              data.forEach(d => {
                console.log(`${d}, ${d.level}`)
                $('#score' + d.level).text(d.score);
                scoreNum = d.score;
                ScoreToStar($('#Stage .stage-star').eq(parseInt(d.level)).find('img'));
              })
            })

            //選關卡-->取資料
            // `http://122.116.13.245:41025/en-games/api/stage/stage/${stageData.stage}`
            // json
            $.get(`http://122.116.13.245:41025/en-games/api/stage/stage/${stageData.stage}`).done(function (data) {
              stageWords = data;
            })
            break;
          case _click.hasClass("stage-item"):
            var pageName = $(this).attr('href');
            stageData.level = $(this).attr('rel'); //記錄點選的難度
            ChangePage(pageName);
            questNum = questRandom(0, 3); //(Normal.Hard)
            scoreNum = 0;
            count=0;
            $('.round-score-num').text(0); //進入關卡前先把分數歸0
            console.log(`stage-itemscoreNum${scoreNum} count${count} `)
            // 再依難度等級建題
            nowLevel();
            break;
          case _click.hasClass("round-back"):
            let _yes = _click.parents().hasClass("play");

            // if (_click.parents().hasClass('round')) {
            console.log("round")
            _time = 0;
            count = 0;
            scoreNum = 0;
            ShowNum = 0;

            let secElement = document.querySelector('.play');
            if (_yes) {
              clearInterval(interval);
              _time = 0;
              count = 0;
              secElement.style.display = 'none';
              secElement.classList.remove('play');
              document.querySelector('.splide').style.display = 'block';
            }
            break;
          case _click.hasClass('RunNext'):
            e.preventDefault();
            CloseLightBox('#ScoreState');
            ChangePage('#home')
            stageData.score = scoreNum; //記錄關卡分數
            sendScore();
            break;
          case _click.hasClass('learn__replay'):
            e.preventDefault();
            let CurrSound = [...document.querySelectorAll('.currSound')];
            CurrSound.forEach((item) => {
              RemoveWordAudio(item);
            })
            ShowNum = 0;
            $('.learn__word-tw').text('')
            $('.learn__word-en').text('')
            ShowLearnTopic();
            break;
        }
      }
    );
  }
  // setInterval(() => {
  //   console.log(interval)
  //   console.log('-----')
  //   console.log(userAns)
  // }, 1000);

  function CountDown() {
    let className;
    if (stageData.level == 0) {
      className = '.round_first';
    } else if (stageData.level == 1) {
      className = '.round_second';
    } else {
      className = '.round_third';
    }
    timer(className);
  }

  function timer(className) {
    interval = setTimeout(() => {
      if (_time <= 0) {
        _time = 0;
        nowLevel();
      } else {
        _time--;
        timer(className);
      }
      document.querySelector(`${className} .progressText`).innerHTML = _time + 's';
      document.querySelector(`${className} .progressBar`).style.width = (stageData.level == 1 || stageData.level == 2) ? _time * 10 / 2 + '%' : _time * 10 + '%';
    }, 1000);
  }
  //新建簡易關卡
  function levelEasyBuild() {
    clearInterval(interval);
    if (count < 4) {
      $('.subject-wordimg img').attr('src', 'images/words/' + stageWords[questNum[count]].eng + '.png');
      $('.subject-wordtext').text(stageWords[questNum[count]].ch)
      $('#ans-eng').val(stageWords[questNum[count]].eng);

      setTimeout(function () {
        $('.card__front').addClass('card-wrap--active');
      }, 250)
      CreateWordAudio(stageWords[questNum[count]].eng);

      var str = '';
      var arr = [];
      var i, j, k;
      for (i = 0; i < 4; i++) {
        str = Math.round(Math.random() * 3);
        for (j = 0; j < arr.length; j++) {
          if (arr[j] == str) {
            arr.splice(j, 1);
            i--;
          }
        }
        arr.push(str);
      }
      for (k = 0; k < arr.length; k++) {
        $('#ans-word-' + arr[k]).text(stageWords[questNum[k]].eng);
      }
      _time = 10;
      CountDown();
    } else {
      console.log('結束');
      _time = 0;
      ScoreNowStateClass();
      setTimeout(function () {
        ScoreToStar($('#ScoreState .state__item-star').find('img'));
        OpenLightBox('#ScoreState');
      }, 1200)
    }
    count++;
  }

  //新建普通填字關卡
  // function levelNormalBuild() {
  //   console.log('normal');
  //   clearInterval(interval);
  //   if (count < 4) {
  //     let words = stageWords[questNum[count]].eng;
  //     let quest = wordsRandom(words);
  //     $('#ans-eng').val(words);
  //     userAns = quest.words;
  //     $('.round_second .subject img').attr('src', 'images/words/' + words + '.png');
  //     $('.round_second .crossword-wordtext').text(stageWords[questNum[count]].ch)
  //     $('.round_second .form-group').html('');
  //     quest.words.map((letter) => {
  //       $('.round_second .form-group').append(`<input class="form-control" type="text" value="${letter}"></input>`);
  //     })
  //     quest.ans.forEach((letter, idx) => {
  //       $('.round_second .letter p').eq(idx).text(letter);
  //     })
  //     //1.取該關卡隨機的單字，及$('#ans-eng').val放入單字
  //     //2.wordsRandom func取得挖空的單字及答案列，讓使用者答案userAns先放入以挖空單字列
  //     //3.題目建置&重製題目input
  //     // $('.round_second .subject img').attr('src', 'images/words/' + 單字 + '.png');
  //     // $('.round_second .crossword-wordtext).text(單字中文)
  //     // $('.round_second .form-group').html('');
  //     //4.取得的挖空單字列及答案列新建
  //     _time = 10;
  //     CountDown();
  //   } else {
  //     console.log('結束');
  //     _time = 0;
  //     ScoreToStar();
  //   }
  //   count++;
  // }


  //新建普通英聽關卡
  function levelNormalBuild() {
    console.log('normal');
    clearInterval(interval);

    if (count < 4) {
      let audioAll = stageWords.map(function (val) {
        return val.eng;
      });
      console.log(`${audioAll} 音`)

      let words = stageWords[questNum[count]].eng;
      $('#ans-eng').val(words);

      $('.round_second .subject img').attr('src', 'images/words/' + words + '.png');
      $('.round_second .crossword-wordtext').text(stageWords[questNum[count]].eng)
      $('.round_second .replay-wrap').html('');

      audioAll.sort().forEach((wordVal, idx) => {
        console.log(`音檔順序${wordVal}`)
        $('.round_second .replay-wrap').append(`<div><button class="replay-wordsplay"></button><audio id="${wordVal}" src="audio/words/${wordVal}.mp3"></audio></div>`);
        $('.round_second .letter input').eq(idx).val(wordVal);
      })

      $('.replay-wordsplay').on('click', function (e) {
        e.preventDefault();
        let soundId = $(this).next().attr('id');
        var sound = document.getElementById(soundId);
        sound.play();
      });
      _time = 20;
      CountDown();
    } else {
      console.log('結束');
      _time = 0;

      ScoreNowStateClass();
      ScoreToStar($('#ScoreState .state__item-star').find('img'));
      OpenLightBox('#ScoreState');
    }
    count++;
  }
  //新建困難關卡
  function levelHardBuild() {
    clearInterval(interval);
    if (count < 4) {
      let words = stageWords[questNum[count]].eng;
      $('#ans-eng').val(words);
      let quests = words.split('');
      $('.round_third .subject img').attr('src', 'images/words/' + words + '.png');
      $('.round_third .crossword-wordtext').text(stageWords[questNum[count]].ch)
      $('.round_third .form-group,.round_third .letter__wrapper').html('');
      CreateWordAudio(stageWords[questNum[count]].eng);
      quests.forEach(function (letter) {
        $('.round_third .form-group').append(`<div class="form-control"></div>`)
      })
      quests.sort().map(letter => {
        $('.round_third .letter__wrapper').append(`<div class="letter"><p>${letter}</p></div>`);
      })
      //1.取該關卡隨機的單字，及$('#ans-eng').val放入單字
      //2.將取得的單字拆分成字母
      //3.題目建置&重製答案
      //$('.round_third .subject img').attr('src', 'images/words/' + 單字 + '.png');
      //$('.round_third .crossword-wordtext).text(中文)
      //$('.round_third .form-group,.round_third .letter__wrapper').html('');
      //4.題目依字母數量迴圈新建
      //5.答案依字母數量新建(可依字母順序排列後再新建)
      _time = 20;
      CountDown();
    } else {
      console.log('結束');
      _time = 0;
      ScoreNowStateClass();
      ScoreToStar($('#ScoreState .state__item-star').find('img'));
      OpenLightBox('#ScoreState');
    }
    count++;
  }

  function CreateWordAudio(word) {
    console.log("creat" + word)
    const audioPlayer = document.querySelector('.listen');
    const sound = document.createElement('audio');
    sound.setAttribute('src', `audio/words/${word}.mp3`);
    sound.setAttribute('id', word);
    sound.classList.add("currSound");

    document.body.appendChild(sound);
    audioPlayer.addEventListener("click", soundHandler(sound));
  }

  function soundHandler(sound) {
    //   sound.pause();
    //   var playPromise = sound.play();
    //   if (playPromise !== undefined) {
    //     playPromise.then(_ => {
    //         sound.play();
    //       })
    //       .catch(err => {
    //         sound.pause();
    //         // soundHandler(sound)
    //       });
    //   }
    let playPromise = new Promise(function (resolve, reject) {
      sound.addEventListener('canplaythrough',function(){
        resolve();
      })
    })
    function palyAll(){
      sound.play();
    }
    Promise.all([playPromise]).then(palyAll());
  }

  function RemoveWordAudio(sound) {
    document.body.removeChild(sound)
  }

  //分數轉換星數
  function ScoreToStar(el) {

    let ScoreTotal = scoreNum //只取自己那關的分數，不然會取用到3個關卡的分數
    ScoreTotal = parseInt(ScoreTotal);

    if (ScoreTotal >= 300) {
      CreateStarImg('h', 3, el);
    } else if (ScoreTotal >= 200 && ScoreTotal < 300) {
      CreateStarImg('h', 2, el);
    } else if (ScoreTotal > 0 && ScoreTotal < 200) {
      CreateStarImg('h', 1, el);
    } else {
      CreateStarImg('n', 3, el);
    }
    console.log(`scoreNum${scoreNum}`)
  }

  // 星星圖
  function CreateStarImg(state = 'n', starNum, el) {
    let star = el;
    let Srctext = 'images/components/star_' + state + '.png';

    for (let i = 0; i < starNum; i++) {
      star.eq(i).attr('src', Srctext);
    }
  }

  function autoFeeback(ansState, className) {
    if (ansState === 'right') {
      document.querySelector('.feekback__box').classList.add(className)
    } else {
      document.querySelector('.feekback__box').classList.add(className)
    }
    feebackAni(className);
  }

  //題目順序亂數(Normal.Hard)
  //(1.此func用來取亂數的題目順序/ 2.limit用來普通關取挖空位置亂數)
  function questRandom(min = 0, max, limit) {
    let quests = [];
    let length = limit ? limit : max + 1;
    for (let i = min; i < length; i++) {
      let amount = parseInt(Math.random() * (max - min + 1) + min);
      if (quests.indexOf(amount) == -1) {
        quests[i] = amount;
      } else {
        i--;
      }
    }
    return quests;
    //傳入設定的最小值min、最大值max、數量限制limit
    //迴圈執行取亂數排列(題目不重複，要判斷如果陣列裡已經有該題目就要重新執行迴圈)
    //回傳題目亂數的結果 return quests;
  }

  //字母亂數(Normal)
  //(此func用來處理普通關卡的題目挖空及答案隨機塞字)
  function wordsRandom(questWords) {
    /*單字挖空*/
    //questWords→為題目單字
    let words = questWords.split('');
    let limit = words.length > 5 ? 3 : 2;
    let space = questRandom(0, words.length - 1, limit);
    let ans = []; //答案字卡
    space.forEach(function (num) {
      ans.push(words[num]);
      words[num] = "";
    })
    //1.將單字拆分成字母陣列
    //2.設定字母長度需挖幾個空位(我是預設>5挖3格，<5挖2格)
    //3.利用questRandom func取得哪幾個位置要挖空
    //4.設定一個答案變數(陣列)，將挖空的字母推入，並再字母陣列的該位置設為""

    /*字母亂數*/
    let wordAll = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    wordAll = wordAll.filter(function (word) {
      return ans.indexOf(word) == -1; //為什麼要return
    });

    let otherAns = questRandom(0, wordAll.length - 1, 4 - ans.length)
    otherAns.forEach(function (num) {
      ans.push(wordAll[num]);
    })
    ans = ans.sort();
    let quest = {
      words,
      ans
    }
    return quest;
    //1.從wordAll移除已經有的字母(答案變數)
    //2.利用questRandom func亂數取得要塞入的字母索引，取數個亂數字母塞進答案陣列
    //3.可利用sort排序一下再回傳
    //最後將挖空的單字及答案亂數包裝成物件回傳 let quest = {words,ans}，return quest;
  }

  //現在難度
  function nowLevel() {
    if (stageData.level == 0) {
      levelEasyBuild();
    } else if (stageData.level == 1) {
      levelNormalBuild();
    } else {
      levelHardBuild();
    }
  }
  //分數顯示
  function scoreShow(ans) {
    if (ans == 'right') {
      autoFeeback('right', 'circle')
      right_btn.play();
      // if 時間分數判斷
      if (_time <= 4) {
        scoreNum += 20
      } else if (_time <= 7 && _time > 4) {
        scoreNum += 80
      } else {
        scoreNum += 100
      }
    } else {
      autoFeeback('wrong', 'false')
      fail_btn.play();
      scoreNum -= 50; //要倒扣
    }
    $('.round-score-num').text(scoreNum);
    $('.state__item-scoreNum').text(scoreNum);
  }

  function sendScore() {
    json = `http://122.116.13.245:41025/en-games/api/stage/stage`;
    $.post(json, stageData).done(function (data) {
      console.log(`stageData＝＝＝＝＝${JSON.stringify(stageData)}`)
      if (data == "success") {
        count = 0;
        scoreNum = 0;
        stageData.score = '';
        // $('.round-score-num').text(scoreNum);
        CreateStarImg('n', 0, $('#ScoreState .state__item-star').find('img'));
        console.log("執行成功");
      }
      console.log(`count${count}`)
      console.log(`scoreNum${scoreNum}`)
      console.log(stageData.score)
    })
  }

  function ShowLearnTopic() {
    let wordlen = stageWords.length;
    let CurrSound = [...document.querySelectorAll('.currSound')];
    CurrSound.forEach((item) => {
      RemoveWordAudio(item);
    })
    console.log(`Learn${stageWords}`)
    let ShowInterval = setInterval(function () {
      if (ShowNum < wordlen) {
        $('.learn__wordimg img').attr('src', 'images/words/' + stageWords[ShowNum].eng + '.png');
        $('.learn__word-tw').text(stageWords[ShowNum].ch)
        $('.learn__word-en').text(stageWords[ShowNum].eng)
        CreateWordAudio(stageWords[ShowNum].eng);
      } else {
        // console.log('done')
        clearInterval(ShowInterval);
      }

      ShowNum++
    }, 1500);
  }

  function ScoreNowStateClass() {
    $('.state__item').removeClass('state-title--failed');
    $('.state__item-face').removeClass('state__item-face--failed');
    if (scoreNum <= 0) {
      $('.state__item').toggleClass('state-title--failed');
      $('.state__item-face').toggleClass('state__item-face--failed');
    }
  }


  /*------↓事件註冊↓-------*/
  //簡單關--點選答
  $('.card').on('click', function () {
    let click_val = $(this).find('.ans-word').text()
    let hidden_val = $('#ans-eng').val();

    $('.card').not(this).find('.card__front').removeClass('card-wrap--active'); //除了自己其餘蓋牌
    //  判斷答案正確與否
    if (hidden_val == click_val) {
      scoreShow('right');
    } else {
      scoreShow('wrong');
    }
    $(this).find('.card__front').addClass('card-wrap--active');
    let CurrSound = [...document.querySelectorAll('.currSound')];
    CurrSound.forEach((item) => {
      RemoveWordAudio(item);
    })
    if ($('body').hasClass('done') || $('body').hasClass('default')) {
      $('body').removeClass('done').removeClass('default');
      setTimeout(function () {
        $('.card__front').toggleClass('card-wrap--active'); //開牌
        levelEasyBuild();
      }, 2000)
    }
  })

  //普通關--點選答案
  // $('.round_second .letter').click(function () {
  //   let click_val = $(this).find('p').text()
  //   let hidden_val = $('#ans-eng').val();
  //   let idx = userAns.indexOf('');
  //   userAns.splice(idx, 1, click_val);
  //   $(`.round_second input`).eq(idx).val(click_val);
  //   console.log(userAns)
  //   if (userAns.indexOf('') == -1) {
  //     let finalAns = userAns.join('');
  //     (hidden_val == finalAns) ? scoreShow('right'): scoreShow('wrong');
  //     userAns.length = 0;
  //     levelNormalBuild();
  //   }
  //   //1.判斷空位""在userAns的索引值，將click_val放進userAns該索引值位置及題目$(`.round_second input`)的空位
  //   //2.判斷如果userAns裡面找不到空位""，則判斷userAns是否與答案相等，相同答對，不同則答錯。
  //   //2-1.userAns清空，執行建立下一題普通題
  // })

  //普通關英聽--點選答案
  $('.round_second .letter').click(function () {
    let click_val = $(this).find("input[name='listenWord']").val();
    let hidden_val = $('#ans-eng').val();
    (hidden_val === click_val) ? scoreShow('right'): scoreShow('wrong');

    if ($('body').hasClass('done') || $('body').hasClass('default')) {
      $('body').removeClass('done').removeClass('default');
      setTimeout(function () {
        levelNormalBuild();
      }, 2000)
    }
  })

  //困難關--點選答案
  $('.round_third').on('click', '.letter', function () {
    let click_val = $(this).find('p').text()
    let hidden_val = $('#ans-eng').val();
    $(this).hide()
    userAns.push(click_val);
    $(`.round_third .form-control`).eq(userAns.length - 1).text(click_val)
    if (userAns.length == hidden_val.length) {
      let finalAns = userAns.join('');
      (finalAns == hidden_val) ? scoreShow('right'): scoreShow('wrong');
      userAns.length = 0;
      if ($('body').hasClass('done') || $('body').hasClass('default')) {
        $('body').removeClass('done').removeClass('default');
        setTimeout(function () {
          levelHardBuild();
        }, 2000)
      }
    }
    //1.將取得的字母推進userAns
    //2.$(`.round_third input`)依位置顯示剛剛點按的字幕
    //3.判斷userAns與hidden_val長度是否相同，如果長度相同就判斷是否視同個單字，同單字答對，不同則判斷錯誤。
    //3-1.userAns清空，執行建立下一題困難題
  })

  $('.listen').on('click', function () {
    let currWord = $('#ans-eng').val();
    $('#' + currWord)[0].play();
  })
  // 開啟學習
  $('.lightbox__learnbox').on('click', function () {
    let _href = $(this).attr('href');
    ChangePage(_href);
    $('.learn__wordimg img').attr('src', '');
    $('.learn__word-tw').text('')
    $('.learn__word-en').text('')
    ShowLearnTopic();
  })
  // 按鈕音效
  $('[data-btnEffect]').on('click', function () {
    click_btn.play();
  })

  function Login() {
    Swal.fire({
      allowOutsideClick: false,
      title: 'Login',
      html: `<input type="text" id="account" class="swal2-input" placeholder="Username" >
    <input type="password" id="password" class="swal2-input" placeholder="Password">`,
      confirmButtonText: 'Sign in',
      focusConfirm: true,
      customClass: {
        title: 'font',
        confirmButton: 'font'
      },
      preConfirm: () => {
        let account = $('#account').val();
        let password = $('#password').val();

        $.ajax({ // 檢證輸入的帳號、密碼。
          url: "http://122.116.13.245:41025/en-games/api/user/user",
          type: "POST",
          dataType: "json",
          data: {
            "account": account,
            "psw": password
          },
          error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR.responseText);
            console.log(textStatus);
            console.log(errorThrown);
          },
          success: function (data) {
            if (data == null || data == undefined || data == "[]") {
              console.log("請重新輸入")
              Swal.fire({
                title: `帳密錯誤或無此帳密，請重新輸入`,
                icon: 'error',
                preConfirm: () => {
                  Login();
                }
              })
            } else {
              stageData.userID = data[0].userID;
              userData = data[0];
              Swal.fire({
                allowOutsideClick: false,
                title: `登入成功，開始遊戲！！`,
                icon: 'success',
              })
            }
          }
        });

      },
    }).then(function (data) {
      if (data.value == "[]") {
        Login();
      }
    })
  }

  function loadAni() {
    const tlLoad = gsap.timeline({
      onStart: function () {
        window.scrollTo(0, 0);
      },
      onComplete: function () {
        Login();
      }
    });
    tlLoad.to('.animate', {
      delay: 2,
      duration: 0.5,
      opacity: 0,
    });
    tlLoad.to('.animation', {
      delay: 1,
      duration: 1,
      y: "100%",
      ease: "power4.out",
    })
    tlLoad.to('.animation', {
      zIndex: -1,
      display: 'none'
    })
    // tlLoad.from('.wrapper-container', {
    //   delay: .25,
    //   duration: .8,
    //   transformOrigin: "center",
    //   scale:0,
    //   opacity: 0,
    //   ease:"elastic.out(1,0.8)",
    //   clearProps: 'all'
    // })
  }

  $(document).ready(function () {
    $.Body = $("body");
    $.Body.MainDataInIt();
    ClickEvent();
    // loadAni();
    // InstructionSplide.mount();
    // init();
  })
})();