//21/11/24
(function () {
  var init = function () {
    ClickEvent();

  }
  let click_btn = document.querySelector('#click');
  let right_btn = document.querySelector('#answer');
  let fail_btn = document.querySelector("#wrong");
  var score = $('.round-score-num')
  var scoreNum = 0;
  var count = 0; //現在題數
  var json = "json/index.json";
  var stageWords; //該關卡單字
  let stageData = { //該關卡資料
    userID: 1,
    stage: '',
    level: '',
    score: ''
  };
  let questNum = []; //題目順序(normal/hard)
  let userAns = []; //使用者答案列(normal/hard)

  var CloseLightBox = function (lbHref) {
    $(".lightbox").removeClass("is-active");
    // $('.lightbox__item').removeClass('is-active');
    $(lbHref).removeClass('is-active');
    $("body").removeClass('noscroll');
    return false;
  };

  function OpenLightBox(lbHref) {
    $(".lightbox__close").click(function () {
      CloseLightBox();
      return false;
    });

    $(".lightbox").addClass("is-active");
    $(lbHref).addClass('is-active');
    $("body").addClass('noscroll');
    return false;
  };

  function ChangePage(page) {
    CloseLightBox();
    $('.splide').hide();
    switch (page) {
      case '#easy':
        $('.round_first').show().addClass('play');
        break;
      case '#normal':
        $('.round_second').show().addClass('play');
        break;
      case '#hard':
        $('.round_third').show().addClass('play');
        break;
    }
  }

  function ClickEvent(e) {
    $.Body.on(
      "click",
      ".lb-open , .stage-item , .round-back ",
      function (e) {
        var _click = $(this);
        switch (true) {
          case _click.hasClass("lb-open"):
            e.preventDefault();
            var lbHref = $(this).attr('href');
            console.log(lbHref)
            // var lbHref = '#Record';
            OpenLightBox(lbHref);

            //選關卡-->取資料
            $.get(json).done(function (data) {
              stageWords = data;
            })
            break;
          case _click.hasClass("stage-item"):
            var pageName = $(this).attr('href');
            stageData.level = $(this).attr('rel');
            ChangePage(pageName);
            questNum = questRandom(0, 3); //(Normal.Hard)
            // 再依難度等級建題
            nowLevel();
            break;
          case _click.hasClass("round-back"):
            let _yes = _click.parent().parent(".round").hasClass("play");
            let secElement = document.querySelector('.play');
            if (_yes === true) {
              secElement.style.display = 'none';
              secElement.classList.remove('play');
              document.querySelector('.splide').style.display = 'block';
              _time = 0;
            }
            break;
        }
      }
    );
  }

  let _time = 10;
  let interval;

  //題目時間倒數
  function CountDown() {
    interval = setInterval(() => {
      document.querySelector('.progressText').innerHTML = _time + 's';
      document.querySelector('.progressBar').style.width = _time * 10 + '%';
      if (_time <= 0) {
        _time = 0;
        userAns.length = 0;
        nowLevel();
      }
      _time--;
      console.log(`剩餘時間${_time}`);
    }, 1000);
  }

  //新建簡易關卡
  function levelEasyBuild() {
    clearInterval(interval);

    if (count < 4) {
      $('.subject-wordimg img').attr('src', 'images/words/' + stageWords[count].eng + '.png');
      $('.subject-wordtext').text(stageWords[count].ch)
      $('#ans-eng').val(stageWords[count].eng);
      for (var i = 0; i < 4; i++) {
        $('#ans-word-' + [i]).text(stageWords[i].eng);
      }
      _time = 10;
      CountDown();
    } else {
      console.log('結束');
      _time = 0;

      ScoreToStar();
    }
    count++;
  }

  //新建普通關卡
  function levelNormalBuild() {
    console.log('normal');
    clearInterval(interval);
    if (count < 4) {
      let words = stageWords[questNum[count]].eng;
      let quest = wordsRandom(words);
      $('#ans-eng').val(words);
      userAns = quest.words;
      $('.round_second .subject img').attr('src', 'images/words/' + words + '.png');
      $('.round_second .crossword-wordtext.font-bold').text(stageWords[questNum[count]].ch)
      $('.round_second .form-group').html('');
      quest.words.map((letter) => {
        $('.round_second .form-group').append(`<input class="form-control" type="text" value="${letter}"></input>`);
      })
      quest.ans.forEach((letter, idx) => {
        $('.round_second .letter p').eq(idx).text(letter);
      })
      _time = 10;
      CountDown();
    } else {
      console.log('結束');
      _time = 0;
      ScoreToStar();
    }
    count++;
  }

  //新建困難關卡
  function levelHardBuild() {
    clearInterval(interval);
    if (count < 4) {
      let words = stageWords[questNum[count]].eng;
      let quests = words.split('');
      $('#ans-eng').val(words);
      $('.round_third .subject img').attr('src', 'images/words/' + words + '.png');
      $('.round_third .crossword-wordtext.font-bold').text(stageWords[questNum[count]].ch)
      $('.round_third .form-group,.round_third .letter__wrapper').html('');
      quests.forEach((letter) => {
        $('.round_third .form-group').append(`<input class="form-control" type="text" value=""></input>`);
      })
      quests.sort().map(letter => {
        $('.round_third .letter__wrapper').append(`<div class="letter"><p>${letter}</p></div>`);
      })
      _time = 10;
      CountDown();
    } else {
      console.log('結束');
      _time = 0;
      ScoreToStar();
    }
    count++;
  }

  //分數轉換星數
  function ScoreToStar() {}

  //題目順序亂數(Normal.Hard)
  function questRandom(min = 0, max, limit) {
    let quests = [];
    let length = limit ? limit : max + 1;
    for (let i = min; i < length; i++) {
      let temp = parseInt(Math.random() * (max - min + 1) + min);
      if (quests.indexOf(temp) == -1) {
        quests[i] = temp;
      } else {
        i--;
      }
    }
    return quests;
  }

  //字母亂數(Normal)
  function wordsRandom(questWords) {
    let words = questWords.split('');
    let limit = words.length > 5 ? 3 : 2;
    let questSpace = questRandom(0, words.length - 1, limit); //抓哪幾個位置要挖空
    let ans = []; //答案字卡
    questSpace.forEach(num => {
      ans.push(words[num]);
      words[num] = "";
    })

    let wordAll = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    wordAll = wordAll.filter(word => ans.indexOf(word) == -1)
    let otherAns = questRandom(0, wordAll.length - 1, 4 - ans.length);
    otherAns.forEach(num => {
      ans.push(wordAll[num]);
    })
    ans = ans.sort();
    // console.log(ans)
    let quest = {
      words,
      ans
    }
    return quest;
  }

  //現在難度
  function nowLevel() {
    if (stageData.level == 0) {
      levelEasyBuild();
    } else if (stageData.level == 1) {
      levelNormalBuild();
    } else {
      levelHardBuild();
    }
  }

  //分數顯示
  function scoreShow(ans) {
    if (ans == 'right') {
      console.log("正確")
      right_btn.play();
      scoreNum += 100;
    } else {
      console.log("錯誤")
      fail_btn.play();
      scoreNum -= 100; //要倒扣?
    }
    score.text(scoreNum)
    stageData.score = scoreNum;
  }

  /*------↓事件註冊↓-------*/

  //簡單關--點選答案
  $('.card').on('click', function () {
    let click_val = $(this).find('.ans-word').text()
    let hidden_val = $('#ans-eng').val();
    //  判斷答案正確與否
    if (hidden_val == click_val) {
      scoreShow('right');
    } else {
      scoreShow('wrong');
    }
    levelEasyBuild();
  })

  //普通關--點選答案
  $('.round_second .letter').click(function () {
    let click_val = $(this).find('p').text();
    let hidden_val = $('#ans-eng').val();
    let idx = userAns.indexOf('');
    userAns.splice(idx, 1, click_val);
    $(`.round_second input`).eq(idx).val(click_val);
    console.log(userAns)
    if (userAns.indexOf('') == -1) { //該題答完 才判斷對錯
      let finalAns = userAns.join('');
      (hidden_val == finalAns) ? scoreShow('right'): scoreShow('wrong');
      userAns.length = 0;
      levelNormalBuild();

    }
  })

  //困難關--點選答案
  $('.round_third').on('click', '.letter', function () {
    let click_val = $(this).find('p').text()
    let hidden_val = $('#ans-eng').val();

    userAns.push(click_val);
    $(`.round_third input`).eq(userAns.length - 1).val(click_val);
    if (userAns.length == hidden_val.length) {
      let finalAns = userAns.join("");
      (finalAns == hidden_val) ? scoreShow('right'): scoreShow('wrong');
      userAns.length = 0;
      levelHardBuild();
    }
  })

  // 按鈕音效
  $('a').on('click', function () {
    click_btn.play();
  })

  $(document).ready(function () {
    $.Body = $("body");
    init();
  })

})();