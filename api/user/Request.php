<?php

/**
 * request
 */
class Request
{
  //允許請求的方式
  private static $method_type = array('get', 'post', 'put', 'patch', 'delete');

  //確認使用者
  public static function checkUser($data)
  {
    try {

      require_once("../connect_db.php");
      if (!$data) {
        // $data = "john123";
      }
      $sql = "select * from users where account=:account and psw=:psw ;";

      $allList = $pdo->prepare($sql);
      $allList->bindValue(':account', $data['account']);
      $allList->bindValue(':psw', $data['psw']);
      $allList->execute();
      if ($allList->rowCount() == 0) {
        return '[]';
      } else {
        $listRow = $allList->fetchAll(PDO::FETCH_ASSOC);
        // return json_encode($listRow);
        // print_r($listRow);
        return $listRow;
      }
    } catch (PDOException $e) {
      echo "例外行號:", $e->getLine(), "<br>";
      echo "錯誤訊息:", $e->getMessage(), "<br>";
    };
  }

  //接收請求---最先進入
  public static function getRequest()
  {
    //請求方法
    $method = strtolower($_SERVER['REQUEST_METHOD']); //$method = get.post...

    if (in_array($method, self::$method_type)) {
      //調用請求方式對應的方法
      $data_name = $method . 'Data';
      return self::$data_name($_REQUEST); //$_REQUEST=>網址後搜尋參數
    }
    return false;
  }

  //GET 獲取資料
  private static function getData($request_data)
  {
    $user_id = (int)$request_data['user'];
    return self::checkUser($user_id);
  }

  // POST /user：確認使用者
  private static function postData($request_data)
  {
    if (!empty($request_data['user'])) {
      return self::checkUser($request_data);
    } else {
      return false;
    }
  }
}
