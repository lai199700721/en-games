<?php

/**
 * request
 */
class Request
{
  //允許請求的方式
  private static $method_type = array('get', 'post', 'put', 'patch', 'delete');

  //測試數據
  // private static $test_class = array(
  //   1 => array('name' => 'test class1', 'count' => 18),
  //   2 => array('name' => 'test class2', 'count' => 20),
  // );

  //取得關卡單字
  public static function getStage($stage)
  {
    try {

      require_once("../connect_db.php");
      if (!$stage) {
        $stage = 1;
      }
      $sql = "select *  from words where stage=:stage;";

      $allList = $pdo->prepare($sql);
      $allList->bindValue(':stage', $stage);
      $allList->execute();
      if ($allList->rowCount() == 0) {
        return '[]';
      } else {
        $listRow = $allList->fetchAll(PDO::FETCH_ASSOC);
        // return json_encode($listRow);
        // print_r($listRow);
        return $listRow;
      }
    } catch (PDOException $e) {
      echo "例外行號:", $e->getLine(), "<br>";
      echo "錯誤訊息:", $e->getMessage(), "<br>";
    };
  }

  //取得關卡各等級分數
  public static function getStageAll($data)
  {
    require("../connect_db.php");
    try {

      $stageAllsql = "select * from stages where userID=:userID and stage=:stage;";

      $stageAllList = $pdo->prepare($stageAllsql);

      $stageAllList->bindValue(':userID', $data['userID']);
      $stageAllList->bindValue(':stage', $data['stage']);
      $stageAllList->execute();

      if ($stageAllList->rowCount() > 0) {
        $stageAllList = $stageAllList->fetchAll(PDO::FETCH_ASSOC);
        return $stageAllList;
      } else if ($stageAllList->rowCount() == 0) {
        return '[]';
      }
    } catch (PDOException $e) {
      echo "例外行號:", $e->getLine(), "<br>";
      echo "錯誤訊息:", $e->getMessage(), "<br>";
    };
  }

  //新增分數
  public static function createScore($data)
  {
    require("../connect_db.php");
    try {

      $createsql = "insert into stages(stageID,userID,stage,level,score)
      values (null,:userID,:stage,:level,:score);";

      $createList = $pdo->prepare($createsql);

      $createList->bindValue(':userID', $data['userID']);
      $createList->bindValue(':stage', $data['stage']);
      $createList->bindValue(':level', $data['level']);
      $createList->bindValue(':score', $data['score']);
      $createList->execute();

      if ($createList->rowCount() > 0) {
        return 'success';
      }
    } catch (PDOException $e) {
      echo "例外行號:", $e->getLine(), "<br>";
      echo "錯誤訊息:", $e->getMessage(), "<br>";
    };
  }

  //查詢是否有關卡分數
  public static function checkStageScore($data)
  {
    try {
      require_once("../connect_db.php");
      $sql = "select userID,stageID,stage,level,score
      from users join stages using(userID)
      where users.userID = :userID and stages.stage=:stage and stages.level=:level;";

      $stageList = $pdo->prepare($sql);
      $stageList->bindValue(':userID', $data['userID']);
      $stageList->bindValue(':stage', $data['stage']);
      $stageList->bindValue(':level', $data['level']);
      $stageList->execute();
      if ($stageList->rowCount() == 0) { //找不到就新增
        return self::createScore($data);
      } else { //找的到就更新
        // die('update');
        // $stageList = $stageList->fetchAll(PDO::FETCH_ASSOC);
        return self::updateScore($data);
      }
    } catch (PDOException $e) {
      echo "例外行號:", $e->getLine(), "<br>";
      echo "錯誤訊息:", $e->getMessage(), "<br>";
    };
  }
  //更新分數
  public static function updateScore($data)
  {
    try {
      require("../connect_db.php");

      $sql = "update stages set stages.score=:score where stages.userID = :userID and stages.stage = :stage and stages.level = :lv; ";

      $modifyData = $pdo->prepare($sql);
      $modifyData->bindValue(':userID',  $data['userID']);
      $modifyData->bindValue(':stage',  $data['stage']);
      $modifyData->bindValue(':lv', $data['level']);
      $modifyData->bindValue(':score', $data['score']);
      $modifyData->execute();
      if ($modifyData->rowCount() > 0) {
        // echo 'success';
        return 'success';
      }
    } catch (PDOException $e) {
      echo "例外行號:", $e->getLine(), "<br>";
      echo "錯誤訊息:", $e->getMessage(), "<br>";
    };
  }

  //接收請求---最先進入
  public static function getRequest()
  {
    //請求方法
    $method = strtolower($_SERVER['REQUEST_METHOD']); //$method = get.post...

    if (in_array($method, self::$method_type)) {
      //調用請求方式對應的方法
      $data_name = $method . 'Data';
      return self::$data_name($_REQUEST); //$_REQUEST=>網址後搜尋參數
    }
    return false;
  }

  //GET 獲取資料
  private static function getData($request_data)
  {
    $stage_id = (int)$request_data['stage']; //獲取該關卡單字
    return self::getStage($stage_id);
  }

  // POST /stage：新建關卡分數
  private static function postData($request_data)
  {
    if (!empty($request_data['stage']) && !empty($request_data['score'])) {
      return self::checkStageScore($request_data);
    } else  if (!empty($request_data['stage'])) {
      return self::getStageAll($request_data);
    } else {
      return false;
    }
  }
}
