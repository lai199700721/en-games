// https://www.youtube.com/watch?v=Lt7fgHkcxOc&list=PLA0YHwTjkRztn3oTKbdseBrymwjsKeMgI&index=3
const {
  src,
  dest,
  watch,
  series,
  parallel
} = require('gulp');

const pug = require('gulp-pug');
const gulpSass = require('gulp-sass')(require('sass')); // 載入 gulp-sass
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const imagemin = require('gulp-imagemin');
const browserify = require("browserify");
const source = require('vinyl-source-stream');

const babel = require('gulp-babel'); // 載入 gulp-babel 套件
const uglify = require('gulp-uglify'); //載入 gulp-uglify 套件
const del = require('del');

//  刪除指定目錄檔案 
function clean() {
  return del(['dist']); // 需刪除檔案或目錄
}

function copyAudio() {
  return src(
    'src/audio/**/*',
    'src/fonts/*',
    'src/json/*',
  ).pipe(dest('./dist/audio/'));
}

function coptFonts() {
  return src(
    'src/fonts/*',
  ).pipe(dest('./dist/fonts/'));
}

function copyJson() {
  return src(
    'src/json/*',
  ).pipe(dest('./dist/json/'));
}

function BuildPug() {
  return src('src/index.pug')
    .pipe(pug({
      pretty: true, //增加代碼可讀性
    }))
    .pipe(dest('./dist/'))
    .pipe(browserSync.stream()); //注入更改內容
}

gulpSass.compiler = require('node-sass');

function BuildStyles() {
  return src('src/scss/*.scss') // 指定要處理的 Scss 檔案目錄
    .pipe(gulpSass({
      outputStyle: 'compressed' //指定輸出型態 壓縮
    }).on('error', gulpSass.logError)) // 編譯 Scss
    .pipe(dest('dist/css'))
    .pipe(browserSync.stream()); // 指定編譯後的 css 檔案目錄
}

function imagrmin() {
  return src('src/images/**')
    .pipe(imagemin([
      imagemin.gifsicle({
        interlaced: true
      }),
      imagemin.mozjpeg({
        quality: 75,
        progressive: true
      }),
      imagemin.optipng({
        optimizationLevel: 5
      }),
      imagemin.svgo({
        plugins: [{
            removeViewBox: true
          },
          {
            cleanupIDs: false
          }
        ]
      })
    ]))
    .pipe(dest('dist/images'))
}

function ScriptBrowserify() {

  return browserify("src/js/main.js")
    .transform("babelify", {
      presets: ["@babel/preset-env"]
    })
    .bundle()
    .pipe(source('main.js'))
    // .pipe(uglify()) // 壓縮並優化 JavaScript
    .pipe(dest('dist/js')) //
    .pipe(browserSync.stream());
}

// function scripts(){
//   return src('src/js/*.js')
//   .pipe(concat('main.js'))
//   .pipe(
//     babel({
//       presets: ['@babel/env'], // 使用 Babel 編譯
//     })
//   )
//   .pipe(uglify()) // 壓縮並優化 JavaScript
//   .pipe(dest('dist/js')) //
//   .pipe(browserSync.stream());
// }


function watchFn() {
  browserSync.init({
    server: {
      baseDir: 'dist',
    },
    online: true,
    tunnel: true,
    port: 8080,
  })
  watch('src/scss/*.scss', BuildStyles)
  watch('src/*.pug', BuildPug)
  watch('src/js/*js', ScriptBrowserify)
  watch('disc/*.html').on('change', browserSync.reload);
};




// exports.BuildPug = BuildPug
// exports.build = series(clean, parallel(BuildPug, BuildStyles));

// 輸出任務 
// 以下這些都是公有任務
// exports.imagrmin=imagrmin//第一個單獨任務. 若要使用就是下gulp imagrmin 就只執行這一個任務
// exports.scripts= ScriptBrowserify
// gulp.series() -> 按照順序進行  // gulp.parallel() -> 同時進行
// exports.default = series(clean, parallel(imagrmin, BuildPug, BuildStyles, copyAudio,coptFonts,copyJson, ScriptBrowserify, watchFn)) //第二個單獨任務，但裡面有一個串好的任務
exports.default = series(BuildPug, BuildStyles, copyAudio, coptFonts, copyJson, ScriptBrowserify, watchFn);